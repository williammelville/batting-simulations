#load the play by play data from the 2018 season 
#get the data download function from the devtools library make sure devtools has been installed 
library(devtools)
source_gist(8892981)
#read in the file 
parse.retrosheet2.pbp(2018)
#read in the data 
library(readr)
data2018 <- read_csv("download.folder/unzipped/all2018.csv", col_names = FALSE)
fields <- read_csv("download.folder/unzipped/fields.csv")
names(data2018) <- fields$Header
#create variable RUNS.SCORED
data2018$RUNS.SCORED <- with(data2018, (BAT_DEST_ID > 3) + 
                           (RUN1_DEST_ID > 3) + (RUN2_DEST_ID > 3) + (RUN3_DEST_ID > 3))
#use the get.state function to define variables STATE and  NEW.STATE, which contains the runners and outs info before and after the play
get.state <- function(runner1, runner2, runner3, outs){
  runners <- paste(runner1, runner2, runner3, sep="")
  paste(runners, outs)
}
RUNNER1 <- ifelse(is.na(data2018[, "BASE1_RUN_ID"]), 0, 1)
RUNNER2 <- ifelse(is.na(data2018[, "BASE2_RUN_ID"]), 0, 1)
RUNNER3 <- ifelse(is.na(data2018[, "BASE3_RUN_ID"]), 0, 1)
data2018$STATE <- get.state(RUNNER1, RUNNER2, RUNNER3, data2018$OUTS_CT)

NRUNNER1 <- with(data2018, as.numeric(RUN1_DEST_ID==1 | BAT_DEST_ID == 1))
NRUNNER2 <- with(data2018, as.numeric(RUN1_DEST_ID==2 | BAT_DEST_ID == 2 | RUN2_DEST_ID==2))
NRUNNER3 <- with(data2018, as.numeric(RUN1_DEST_ID==3 | RUN2_DEST_ID==3 | RUN3_DEST_ID==3 | BAT_DEST_ID == 3))
NOUTS <- with(data2018, OUTS_CT + EVENT_OUTS_CT)
data2018$NEW.STATE <- get.state(NRUNNER1, NRUNNER2, NRUNNER3, NOUTS)

#focus on plays where there is a change in the state or in runs scored 
data2018 <- subset(data2018, (STATE != NEW.STATE) | (RUNS.SCORED > 0))
#We only want complete innings, so restrict to innings where there are three outs 
library(plyr)
#first define a new variable HALF.INNING 
data2018$HALF.INNING <- with(data2018, paste(GAME_ID, INN_CT, BAT_HOME_ID))
data.outs <- ddply(data2018, .(HALF.INNING), summarize, Outs.Inning = sum(EVENT_OUTS_CT))
data2018 <- merge(data2018, data.outs)
#Now we can restrict the data to just complete three out innings 
data2018C <- subset(data2018, Outs.Inning == 3)
#Remove non batting plays as well 
data2018C <- subset(data2018, BAT_EVENT_FL == TRUE)
#Adjust NEW.STATE so that the baserunners aren't recorded when there's three outs 
library(car)
data2018C$NEW.STATE <- recode(data2018C$NEW.STATE, 
                            "c('000 3', '100 3', '010 3', '001 3',
                               '110 3', '101 3', '011 3', '111 3')='3'")
#define the possible states 
states <- list('000 0', '000 1', '000 2', '001 0', '001 1', '001 2',
               '010 0', '010 1', '010 2', '011 0', '011 1', '011 2',
               '100 0', '100 1', '100 2', '101 0', '101 1', '101 2', 
               '110 0', '110 1', '110 2', '111 0', '111 1', '111 2')
#Since some of these hitters never hit in some states in 2018, I'll need the average transition matrix for all batters in 2018 
T.matrix <- with(data2018C, table(STATE, NEW.STATE))
Avg <- prop.table(T.matrix, 1)
Avg <- rbind(Avg, c(rep(0,24), 1))
#Now define a function which takes a retrosheet player id and returns that player's Transition matrix 
get_matrix <- function(playerid){
  #take a subset of the data where BAT_ID == playerid 
  player_at_bats <- subset(data2018C, BAT_ID == playerid)
  #initialize new_matrix as Avg so that they're the same size 
  #This also means that if a player never batted in a state, his transition matrix in that spot is the MLB average 
  new_matrix <- T.matrix
  for(state1 in states){
    for(state2 in c(states, '3')){
      #loop through the rows and columns of new_matrix
      #assign new_matrix[state1, state2] to the sum of the number of times 
      #that state1 resulted in state2 for the given player 
      new_matrix[state1, state2] = nrow(subset(player_at_bats, STATE == state1 & NEW.STATE == state2))
    }
  }
  new_transition <- prop.table(new_matrix,1)
  #now we loop again through the values to replace null values 
  for(state1 in states){
    for(state2 in c(states, '3')){
      #If the player never batted in that state replace it with the average value 
      if(is.nan(new_transition[state1, state2]) | is.null(new_transition[state1, state2]) | is.na(new_transition[state1, state2])){
        new_transition[state1, state2] <- Avg[state1, state2]
    }
  }
  }
  return(new_transition)
}
#define the transition matrices for the player of interest 
gamel <- get_matrix('gameb001')
dgordon <- get_matrix('gordd002')
jsegura <- get_matrix('seguj002')
kseager <- get_matrix('seagk001')
mzunino <- get_matrix('zunim001')
ncruz <- get_matrix('cruzn002')
rcano <- get_matrix('canor001')
rhealy <- get_matrix('healr001')
gheredia <- get_matrix('hereg002')
cmaybin <- get_matrix('maybc001')
dspan <- get_matrix('spand001')
mhaniger <- get_matrix('hanim001')
#Now we need to define a runs matrix, that gives the runs scored for each pair of out bases state 
#first define a function that returns the sum of runners and outs 
count.runners.outs <- function(s){
  sum(as.numeric(strsplit(s, "")[[1]]), na.rm = TRUE)
}
#apply this funcion to all possible states 
runners.outs <- sapply(dimnames(Avg)[[1]], count.runners.outs)[-25]
#finally define the runs matrix 
Runs <- outer(runners.outs + 1, runners.outs, FUN="-")
dimnames(Runs)[[1]] <- dimnames(Avg)[[1]][-25]
dimnames(Runs)[[2]] <- dimnames(Avg)[[1]][-25]
Runs <- cbind(Runs, rep(0,24))

#finally write all of the transition matrixes to csv files so I can work in python, which is easier for me 
write.csv(cmaybin, file='cmaybin.csv')
write.csv(dgordon, file = 'dgordon.csv')
write.csv(gamel, file='gamel.csv')
write.csv(gheredia, 'gheredia.csv')
write.csv(jsegura, 'jsegura.csv')
write.csv(kseager, 'kseager.csv')
write.csv(mzunino, 'mzunino.csv')
write.csv(ncruz, 'ncruz.csv')
write.csv(rcano, 'rcano.csv')
write.csv(rhealy, 'rhealy.csv')
write.csv(dspan, 'dspan.csv')
write.csv(mhaniger, 'mhaniger.csv')
write.csv(Avg, 'average2018.csv')
write.csv(Runs, 'Runs.csv')
