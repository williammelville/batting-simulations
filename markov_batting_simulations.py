'''
Will Melville
Program to simulate baseball games given a lineup. Requires Individual transition
matrices created in R and downloaded from a csv file into a numpy array
'''

import numpy as np
from numba import jit
from itertools import permutations

#define the list of baserunner outs states, and the list of possible new states
#these lists aren't actually needed in the following functions, but if someone
#wanted to know what state[4] means, they can use this to see that it means '001 1'
states = ['000 0', '000 1', '000 2', '001 0', '001 1', '001 2',
               '010 0', '010 1', '010 2', '011 0', '011 1', '011 2',
               '100 0', '100 1', '100 2', '101 0', '101 1', '101 2',
               '110 0', '110 1', '110 2', '111 0', '111 1', '111 2']
newstates = states + ['3']

#load in the transition matrices. skip the first row and column, which are just the states
#first define the columns that we want to load
cols = [i for i in range(1, len(newstates)+1)]
cols = tuple(cols)
cmaybin = np.loadtxt('cmaybin.csv', delimiter = ',', skiprows = 1, usecols = cols)
dgordon = np.loadtxt('dgordon.csv', delimiter = ',', skiprows = 1, usecols = cols)
bgamel = np.loadtxt('gamel.csv', delimiter = ',', skiprows = 1, usecols = cols)
gheredia = np.loadtxt('gheredia.csv', delimiter = ',', skiprows = 1, usecols = cols)
jsegura = np.loadtxt('jsegura.csv', delimiter = ',', skiprows = 1, usecols = cols)
kseager = np.loadtxt('kseager.csv', delimiter = ',', skiprows = 1, usecols = cols)
mzunino = np.loadtxt('mzunino.csv', delimiter = ',', skiprows = 1, usecols = cols)
ncruz = np.loadtxt('ncruz.csv', delimiter = ',', skiprows = 1, usecols = cols)
rcano = np.loadtxt('rcano.csv', delimiter = ',', skiprows = 1, usecols = cols)
rhealy = np.loadtxt('rhealy.csv', delimiter = ',', skiprows = 1, usecols = cols)
dspan = np.loadtxt('dspan.csv', delimiter = ',', skiprows = 1, usecols = cols)
mhaniger = np.loadtxt('mhaniger.csv', delimiter = ',', skiprows = 1, usecols=cols)
#now load the runs matrix
runs_matrix = np.loadtxt('Runs.csv', delimiter = ',', skiprows = 1, usecols = cols)

#define a dictionary that maps the players' names to their matrices
name_to_matrix = {'Cameron Maybin': cmaybin, 'Dee Gordon': dgordon, 'Ben Gamel': bgamel,
                'Guillermo Heredia': gheredia, 'Jean Segura': jsegura, 'Kyle Seager': kseager,
                'Mike Zunino': mzunino, 'Nelson Cruz': ncruz, 'Robinson Cano': rcano,
                'Ryon Healy': rhealy, 'Denard Span': dspan, 'Mitch Haniger': mhaniger}

def names(lineup):
    '''This function takes a list of transition matrices, and uses the matrix_to_name
    dictionary to return the lineup as a list of names.'''
    named_lineup = []
    names = list(name_to_matrix.keys())
    for matrix in lineup:
        for name in names:
            #loop through the names in name_to_matrix until the corresponding value
            #matches the matrix. Then append that name to the list of names
            if np.all(name_to_matrix[name] == matrix):
                named_lineup.append(name)
                break
    return named_lineup

@jit(nopython = False, locals=dict(games='int64', inning='int64', runs='int64', batter = 'int64',
                                state = 'int64', newstate='int64'))
def batting_sim(lineup, total_games = 162):
    '''This function takes a list of 9 transition matrices (lineup) as input and
    simulates total_games (default value of 162) of batting, keeping track of
    the runs scored. Returns the runs scored over the total_games.'''
    #initialize game, inning, and runs
    games = 1
    inning = 1
    runs = 0
    #simulate total_games games
    while games <= total_games:
        #simulate 9 innings. The first batter of every game is batter 0
        batter = 0
        innings = 1
        while innings <= 9:
            #the first state is '000 0', which is index 0
            state = 0
            #simulate until there are three outs, which is the 24 index state
            while state != 24:
                #get the new state
                newstate = np.nonzero(np.random.multinomial(1, lineup[batter][state, :]))[0][0]
                #add the runs to the runs total
                runs += runs_matrix[state, newstate]
                #iterate to the next batter
                batter += 1
                #if batter = 9, we've made it through the lineup so recycle to 0
                if batter == 9:
                    batter = 0
                #reassign state to newstate
                state = newstate
            #the inning ended, so iterate innings
            innings += 1
        #the game ended so iterate games
        games += 1
    #all games have been played
    return runs

def optimal_lineup(players):
    '''Function that takes a list of 9 transition matrices. It finds all of the permutations
    of the lineup then tests each lineup using season_sim. Returns the ordering
    of the nine players that scored the most runs. This function takes about
    10 minutes to run even after the first function call where numba.jit compiles
    it.'''
    #initialize max_runs
    max_runs = 0
    #permute the players to get all possible lineups
    possible_lineups = list(permutations(players))
    #now test each lineup
    for lineup in possible_lineups:
        #find the runs scored for the current lineup in 10 games
        #if I try it with the default value of 162 games, it takes hours to run
        lineup_runs = batting_sim(list(lineup), 10)
        #if lineup_runs is greater than max runs, change max runs and optimal lineup index
        if lineup_runs > max_runs:
            max_runs = lineup_runs
            optimal = list(lineup)
        else:
            continue
    #return the optimal lineup as names, not matrices
    return names(optimal)

def compare_two_lineups(lineup1, lineup2, N = 100):
    '''This function takes two lineups as input. It runs batting_sim N times for
    each lineup and then returns the average runs scored in a season over the N
    tests for each lineup'''
    lineup1_runs = 0
    lineup2_runs = 0
    for _ in range(N):
        #simulate a season for each lineup N times. Add the runs to the appropriate total
        lineup1_runs += batting_sim(lineup1)
        lineup2_runs += batting_sim(lineup2)
    #return the average of each lineup
    return lineup1_runs / N, lineup2_runs / N
